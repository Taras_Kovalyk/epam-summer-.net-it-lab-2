using System;
using System.Collections.Generic;

namespace ConsoleApplication3
{
    class Program
    {
        class Polynomial
        {
            private int degree;
            private double[] coeficients;

            public Polynomial()
            {

            }

            public Polynomial(int degree, params double[] coeficients)
            {
                this.degree = degree;
                this.coeficients = new double[this.degree + 1];
                this.coeficients = coeficients;
            }
            
            public double Argument(double arg)
            {
                double a = 0;
                double currentDegree = degree;
                for(int i = 0; i<coeficients.Length; i++)
                {
                    a += Math.Pow(arg, currentDegree) * coeficients[i];
                    currentDegree--;
                }
                return a;
            }

            public static Polynomial AddPolynomes(Polynomial a, Polynomial b)
            {
               int d=0;
               List<double> temporaryList = new List<double>();
               if (a.degree > b.degree)
                {
                    d = a.degree;
                    for(int i=a.degree; i>0; i--)
                    {
                        for(int j=b.degree; j>0; j--)
                        {
                            if (i==j)
                            {
                                temporaryList.Add(a.coeficients[i-1] + b.coeficients[j-1]);
                            }
                        }
                    }

                    temporaryList.Reverse();

                    for (int i = b.degree; i < a.degree; ++i)
                    {
                        temporaryList.Insert(0, a.coeficients[i]);
                    }
                }
                else
                {
                    d = b.degree;
                    for (int i = b.degree; i > 0; i--)
                    {
                        for (int j = a.degree; j > 0; j--)
                        {
                            if (i == j)
                            {
                                temporaryList.Add(a.coeficients[i - 1] + b.coeficients[j - 1]);
                            }
                        }
                    }

                    temporaryList.Reverse();

                    for (int i = a.degree; i < b.degree; ++i)
                    {
                        temporaryList.Insert(0, b.coeficients[i]);
                    }
                }

                Polynomial resultPolynomial = new Polynomial(d, temporaryList.ToArray());
                return resultPolynomial;
            }

            public static Polynomial SubstractPolynomes(Polynomial a, Polynomial b)
            {
                int d = 0;
                List<double> temporaryList = new List<double>();
                if (a.degree > b.degree)
                {
                    d = a.degree;
                    for (int i = a.degree; i > 0; i--)
                    {
                        for (int j = b.degree; j > 0; j--)
                        {
                            if (i == j)
                            {
                                temporaryList.Add(a.coeficients[i - 1] - b.coeficients[j - 1]);
                            }
                        }
                    }

                    temporaryList.Reverse();

                    for (int i = b.degree; i < a.degree; ++i)
                    {
                        temporaryList.Insert(0, -a.coeficients[i]);
                    }
                }
                else
                {
                    d = b.degree;
                    for (int i = b.degree; i > 0; i--)
                    {
                        for (int j = a.degree; j > 0; j--)
                        {
                            if (i == j)
                            {
                                temporaryList.Add(a.coeficients[i - 1] - b.coeficients[j - 1]);
                            }
                        }
                    }

                    temporaryList.Reverse();

                    for (int i = a.degree; i < b.degree; ++i)
                    {
                        temporaryList.Insert(0, -b.coeficients[i]);
                    }
                }

                Polynomial resultPolynomial = new Polynomial(d, temporaryList.ToArray());
                return resultPolynomial;
            }

            public override string ToString()
            {
                string result = "";
                double currentDegree = degree;
                for (int i = 0; i < coeficients.Length; i++)
                {
                    result += coeficients[i] + "x^" + (currentDegree) + " + ";
                    currentDegree--;

                    if(i+1 == coeficients.Length)
                    {
                        result += "0";
                    }
                }

                return result;
            }

        }
        static void Main(string[] args)
        {
            Console.WriteLine("***************Addition polynomial***************");
            Polynomial myPolynomial1 = new Polynomial(8, 1,1,1,1,1,1,1,1);
            Console.WriteLine("myPolinomial1 : {0}", myPolynomial1.ToString());

            Polynomial myPolynomial2 = new Polynomial(5, 5,5,5,5,5);
            Console.WriteLine("myPolinomial2 : {0}", myPolynomial2.ToString());

            Polynomial AdditionPolinomial = Polynomial.AddPolynomes(myPolynomial1, myPolynomial2);
            Console.WriteLine("Addition : {0}", AdditionPolinomial.ToString());



            Console.WriteLine("\n***************Getting value by argument***************");
            Polynomial myPolynomial3 = new Polynomial(3, 1,1,1);
            Console.WriteLine("myPolinomial3 : {0}", myPolynomial3.ToString());
            Console.WriteLine("myPolynomial3 with argument 2 : {0}", myPolynomial3.Argument(2));



            Console.WriteLine("\n***************Substraction polynomial***************");
            Polynomial myPolynomial4 = new Polynomial(6, 1,1,10,1,1,1);
            Console.WriteLine("myPolinomial4 : {0}", myPolynomial4.ToString());

            Polynomial myPolynomial5 = new Polynomial(5, 5,5,5,5,5);
            Console.WriteLine("myPolinomial5 : {0}", myPolynomial5.ToString());

            Polynomial substractionPolynomial = Polynomial.SubstractPolynomes(myPolynomial5, myPolynomial4);
            Console.WriteLine("Substraction : {0}", substractionPolynomial.ToString());
        }
    }
}
